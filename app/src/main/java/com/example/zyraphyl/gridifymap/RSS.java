package com.example.zyraphyl.gridifymap;

public class RSS {
    private String MAC;
    private int rssVal;
    private int frequency;
    private int grid;

    public RSS(String MAC, int rssVal, int frequency, int grid) {
        this.MAC = MAC;
        this.rssVal = rssVal;
        this.frequency = frequency;
        this.grid = grid;
    }

    public String getMAC() {
        return MAC;
    }

    public int getRssVal() {
        return rssVal;
    }

    public int getFrequency() {
        return frequency;
    }

    public int getGrid() {
        return grid;
    }

    public void setMAC(String MAC) {
        this.MAC = MAC;
    }

    public void setRssVal(int rssVal) {
        this.rssVal = rssVal;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public void setGrid(int grid) {
        this.grid = grid;
    }
}
