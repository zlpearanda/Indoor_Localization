package com.example.zyraphyl.gridifymap;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final  int RESULT_LOAD_IMAGE = 1;
    private static final int RESULT_SIGNAL_LIST = 2;

    private SharedPreferences sharedPref ;
    private String path,area_name,building_name,floor_num,room_name;
    private EditText area,building,floor,room;
    private JSONObject signals,json_object;
    private Button gather_data,submit;
    private ImageView select_image;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPref = getApplicationContext().getSharedPreferences("SERVER", 0);
        bindViews();
        setViewActions();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.server,menu);
        return true;
    }

    private void bindViews() {
        area = (EditText) findViewById(R.id.area_edittext);
        building = (EditText) findViewById(R.id.building_edittext);
        floor = (EditText) findViewById(R.id.floor_edittext);
        room = (EditText) findViewById(R.id.room_edittext);
        gather_data = (Button) findViewById(R.id.gather_button);
        submit = (Button) findViewById(R.id.submit_button);
        select_image = (ImageView) findViewById(R.id.select_image);
    }

    private void setViewActions() {
        select_image.setOnClickListener(this);
        gather_data.setOnClickListener(this);
        submit.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog.Builder server_builder = new AlertDialog.Builder(MainActivity.this);
        View server_dialog = getLayoutInflater().inflate(R.layout.server_dialog,null);
        final EditText server_field = (EditText) server_dialog.findViewById(R.id.server_field);

        switch (item.getItemId()){
            case R.id.show_server:
                server_builder.setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                });
                server_field.setText(sharedPref.getString("Server_Address",null));
                server_field.setEnabled(false);
                server_builder.setView(server_dialog);
                final AlertDialog show_dialog = server_builder.create();
                show_dialog.show();

                return true;
            case R.id.change_server:
                server_field.setText(sharedPref.getString("Server_Address",null));
                server_builder.setPositiveButton("Change", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                server_builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                server_builder.setView(server_dialog);
                final AlertDialog alert_dialog = server_builder.create();
                alert_dialog.show();

                alert_dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String server_address = server_field.getText().toString();
                        if(!server_address.isEmpty() ){

                            changeServer(server_address);

                        }else{
                            Toast.makeText(MainActivity.this, "Server address not changed", Toast.LENGTH_SHORT).show();
                        }
                        alert_dialog.dismiss();
                    }
                });
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    public void onClick(View v) {
        switch(v.getId()){
            case R.id.select_image:
                selectImage(v);
                break;
            case R.id.gather_button:
                callGatherSignal(v);
                break;
            case R.id.submit_button:
                submitToServer(v);
                break;
        }
    }

    public void selectImage(View v){
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent,RESULT_LOAD_IMAGE);
    }

    public void callGatherSignal(View v){
        if(path!=null){
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            View dialog_view = getLayoutInflater().inflate(R.layout.grid_size_dialog,null);
            final EditText dialog_row = (EditText) dialog_view.findViewById(R.id.rowSize);
            final EditText dialog_column = (EditText) dialog_view.findViewById(R.id.columnSize);

            builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.setView(dialog_view);
            final AlertDialog alert_dialog = builder.create();
            alert_dialog.show();

            alert_dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String row_size = dialog_row.getText().toString();
                    String column_size = dialog_column.getText().toString();
                    if(!column_size.isEmpty() && !row_size.isEmpty()){
                        Toast.makeText(MainActivity.this, "Fields Complete", Toast.LENGTH_SHORT).show();
                        //call another window and pass column size and row size and image
                        alert_dialog.dismiss();
                        Intent nextActivityIntent = new Intent(MainActivity.this,GatherSignal.class);
                        nextActivityIntent.putExtra("path",path);
                        nextActivityIntent.putExtra("rows",row_size);
                        nextActivityIntent.putExtra("columns",column_size);
                        startActivityForResult(nextActivityIntent,RESULT_SIGNAL_LIST);

                    }else{
                        Toast.makeText(MainActivity.this, "Please fill in the fields", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
    public void changeServer(String server){

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("Server_Address", server);
        editor.apply();
    }
    public String createJson(){
        area_name=area.getText().toString();
        building_name=building.getText().toString();
        floor_num=floor.getText().toString();
        room_name=room.getText().toString();
        json_object = new JSONObject();

        if(!area_name.isEmpty() && !building_name.isEmpty() && !floor_num.isEmpty() && !room_name.isEmpty()){
            Log.wtf("createJSon","if valid");
            try {
                json_object.put("Area",area_name);
                json_object.put("Building",building_name);
                json_object.put("Floor",floor_num);
                json_object.put("Room",room_name);
                json_object.put("signals",signals);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return json_object.toString();
        }else{
            Toast.makeText(MainActivity.this, "Please fill in the fields", Toast.LENGTH_SHORT).show();
        }
        return null;
    }

    @SuppressLint("StaticFieldLeak")
    private void submitToServer(View v) {
        final String json = createJson();
        new AsyncTask<Void,Void,String>(){

            @Override
            protected String doInBackground(Void... voids) {
                return sendData(json);
            }

            @Override
            protected void onPostExecute(String s) {
                Toast.makeText(MainActivity.this, s, Toast.LENGTH_LONG).show();
            }
        }.execute();

    }
    private String sendData(String json){

        String address = sharedPref.getString("Server_Address",null);
        HttpPost post = new HttpPost(address);
        try {
            Log.wtf("sendData","sent");
            StringEntity stringEntity = new StringEntity(json);
            post.setEntity(stringEntity);
            post.setHeader("Content-type","application/json");

            DefaultHttpClient client = new DefaultHttpClient();
            BasicResponseHandler handler = new BasicResponseHandler();

            String response = client.execute(post,handler);
            Log.wtf("sendData",response);
            return response;
        } catch (UnsupportedEncodingException e) {
            Log.d("SendingJson",e.toString());
        } catch (ClientProtocolException e) {
            Log.d("SendingJson",e.toString());
        } catch (IOException e) {
            Log.d("SendingJson",e.toString());
        }
        return "Unable to connect to server";
    }

    public String getRealPathFromURI(Uri contentUri) {

        // can post image
        String [] proj={MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery( contentUri,
                proj, // Which columns to return
                null,       // WHERE clause; which rows to return (all rows)
                null,       // WHERE clause selection arguments (none)
                null); // Order-by clause (ascending by name)
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();

        return cursor.getString(column_index);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null){
            Uri selectedImage = data.getData();
            select_image.setImageURI(selectedImage);
            path = getRealPathFromURI(selectedImage);
        } else if (requestCode==RESULT_SIGNAL_LIST && resultCode==RESULT_OK && data != null){
            try {
                signals = new JSONObject(data.getStringExtra("signals"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

}
