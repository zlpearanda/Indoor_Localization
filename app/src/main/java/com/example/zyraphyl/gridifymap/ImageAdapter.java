package com.example.zyraphyl.gridifymap;

import java.util.ArrayList;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;


//The adapter class associated with the ChunkedImageActivity class
public class ImageAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<Bitmap> imageChunks;
    private int imageWidth, imageHeight,imageDimension;
    private GatherSignal gatherSignal;
    //constructor
    public ImageAdapter(Context c, ArrayList<Bitmap> images, int imageDimension, GatherSignal gatherSignal){
        mContext = c;
        imageChunks = images;
        imageWidth = images.get(0).getWidth();
        imageHeight = images.get(0).getHeight();
        this.imageDimension=imageDimension;
        this.gatherSignal = gatherSignal;
    }

    @Override
    public int getCount() {
        return imageChunks.size();
    }

    @Override
    public Object getItem(int position) {
        return imageChunks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ImageView image;
        if(convertView == null){
            image = new ImageView(mContext);
//            LayoutInflater inflater = LayoutInflater.from(mContext);
//            view1 = inflater.inflate(R.layout.activity_gather_signal,null);

            final float scale = mContext.getResources().getDisplayMetrics().density;
            int pixels = (int) (imageDimension* scale + 0.5f);
            image.setLayoutParams(new LinearLayout.LayoutParams(pixels, pixels));
            image.setPadding(2, 1, 2, 1);

            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gatherSignal.scanWifi(position);

                }
            });
        }else{
            image = (ImageView) convertView;
        }
        image.setImageBitmap(imageChunks.get(position));
        return image;
    }
}